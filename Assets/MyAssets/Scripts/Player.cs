﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    Rigidbody rb;
    SteamVR_ControllerManager controllerManager;
    VRTK.VRTK_ControllerEvents eventsLeft, eventsRight;
    //CharacterController characterController;

    public float speed = 10f;
    int lastInputD = 0;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        controllerManager = GetComponent<SteamVR_ControllerManager>();
        //characterController = GetComponent<CharacterController>();
        eventsLeft = controllerManager.left.GetComponent<VRTK.VRTK_ControllerEvents>();
        eventsRight = controllerManager.right.GetComponent<VRTK.VRTK_ControllerEvents>();
        eventsLeft.TriggerPressed += EventsLeft_TriggerPressed;
        eventsRight.TriggerPressed += EventsRight_TriggerPressed;
        eventsLeft.TriggerReleased += Events_TriggerReleased;
        eventsRight.TriggerReleased += Events_TriggerReleased;
    }

    private void Events_TriggerReleased(object sender, VRTK.ControllerInteractionEventArgs e)
    {
        InputDirection(0);
    }

    private void EventsRight_TriggerPressed(object sender, VRTK.ControllerInteractionEventArgs e)
    {
        InputDirection(-1);
    }

    private void EventsLeft_TriggerPressed(object sender, VRTK.ControllerInteractionEventArgs e)
    {
        InputDirection(1);
    }

    void InputDirection(int d)
    {
        lastInputD = d;
    }

    // Update is called once per frame
    void Update()
    {
        rb.MovePosition(new Vector3(lastInputD, 0, 1) * speed * Time.deltaTime);
        //characterController.Move(new Vector3(1, 0, lastInputD) * speed * Time.deltaTime);
        //transform.Translate(new Vector3(0, 0, horizontalDirection.x) * speed * Time.deltaTime);
    }
}
